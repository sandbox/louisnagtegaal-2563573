<?php
/**
 * @file
 * Settings for boosting search results by specific taxonomy terms
 */

/**
 * Settings form for taxonomies and bundle types
 */

function _solr_tag_booster_select_taxonomies() {
  $defaults = unserialize(variable_get('solr_tag_booster_taxonomies', ''));
  $form = array();
  $options = _solr_tag_booster_get_taxonomies();

  $form['solr_tag_booster_taxonomies'] = array(
    array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => t('Select taxonomies whose tags must get boost values'),
      '#default_value' => $defaults,
    ),
  );
  $min_val = variable_get('solr_tag_booster_min_boost', -10);
  $form['solr_tag_booster_min_boost'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimal boost value'),
    '#size' => 5,
    '#default_value' => $min_val,
  );

  $max_val = variable_get('solr_tag_booster_max_boost', 10);
  $form['solr_tag_booster_max_boost'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum boost value'),
    '#size' => 5,
    '#default_value' => $max_val,
  );

  $form['bundletypes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select contenttypes to boost'),
    '#description' => t('Only use positive integer values'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // get all bundles
  $bundles = field_info_bundles('node');
  // get boost values
  $bundle_boosts = unserialize(variable_get('solr_tag_booster_bundles', ''));
  foreach ($bundles as $key => $bundle) {
    $value = 0 ;
    if (isset($bundle_boosts[$key]) && !empty($bundle_boosts[$key])) {
      $value = $bundle_boosts[$key];
    }
    $form['bundletypes'][$key] = array(
      '#type' => 'textfield',
      '#title' => t('Boost for @name', array('@name' => $bundle['label'])),
      '#description' => t('Set boost for @name.', array('@name' => $bundle['label'])),
      '#size' => 5,
      '#default_value' => $value,
    );
  }

  $form = system_settings_form($form);

  array_unshift($form['#submit'], 'solr_tag_booster_setting_submit');
  return $form;
}

/**
 * Submit handler for module-settings form on 'admin/config/search/solr_tag_booster'.
 * Save serialized data
 */
function solr_tag_booster_setting_submit($form, &$form_state) {
  $values = $form['solr_tag_booster_taxonomies'][0]['#value'];
  variable_set('solr_tag_booster_taxonomies', serialize($values));
  $bundle_boosts = array();
  // get all bundles
  $bundles = field_info_bundles('node');
  // loop over all bundles
  foreach ($bundles as $key => $bundle) {
    // if value is set
    if (isset($form_state['values'][$key]) && !empty($form_state['values'][$key])) {
      // add it to array with boost values
      $bundle_boosts[$key] = $form_state['values'][$key];
    }
  }
  variable_set('solr_tag_booster_bundles', serialize($bundle_boosts));
}


/**
 * Helper function to get all taxonomies as options
 */
function _solr_tag_booster_get_taxonomies() {
  $tax_arr = array();
  $all_taxs = taxonomy_get_vocabularies();
  foreach ($all_taxs as $tax) {
    $tax_arr[$tax->vid] = $tax->name;
  }
  return $tax_arr;
}
