This module makes it posisible when using the Apache Solr backend (either with Search API or the Apache Solr module) add boost values to certain taxonomy tags.

Includes an admin interface to select which taxonomies to use and alters the taxonomy overview pages to set the boost value.
