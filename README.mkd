#Solr tag boost

##Description

This modules makes it possible to boost search-results from Apache Solr for nodes which have a certain taxonomy-term
assigned. One use case would the a site in which documents (PDF's, text, scans) are uploaded and where certain types of
documents (e.q. 'Emergency plans') should have a higher Solr ranking then others (e.g. 'Newspaper clippings'). If
there are a lot of such types, creating different content types for each of them would complicate the site structure.

With this module a taxonomy with the document-types could be added and each type could be given a specific boost value.

The module adds boost values on query-time, so if all the necessary data is already indexed re-indexing is not necessary
if the module is enable. But see the remark under 'Prerequisites'.

##Why not sorting

Simply sorting the search results (either by sorting the view when using Search API or by adding a sort to the Solr
query) could be a solution but this would certainly give other results. If, for example, one would add an extra weight
field to the taxonomy terms and sort the view on this value, this sorting would not take into account the sorting
on relevance by Apache Solr. With this module all default boosting on titles, tags etc are kept.


##Installation

Download module and enable it in the module overview

Go to the configuration page on

    /admin/config/search/solr_tag_booster

and enable the taxonomies you want to use. The _Minimal boost value_ and _Maximum boost value_ fields are common for
all of the taxonomies and must be integers.

Go to the taxonomy term overview page of one of the taxonomies you have enabled. In the overview an extra column
'SOLR BOOST' is shown in which you can set the boost value. Do not forget to save the page after setting the value.

##Dependencies

The module works with both the "Search API" and "ApacheSolr" modules.

## Negative boost values

In Solr you can not give a negative boost. If you set _Minimal boost value_ to a negative number and assign a taxonomy
term a negative boost value, in the query that is sent to Solr the actual boost will be that all items that do *not*
have this value will get a positive boost of the absolute boost-value. So if you have a term 'not important' which you
give a boost value of -10, in the Solr query all the items who do not have this term will get a boost of 10.

Most of the time it will be easier to just use positive boost-values (set _Minimal boost value_ to zero or higher).

##Prerequisites

###Index field
Make sure that the term-reference field in which the taxonomy is used is indexed.

Search API:

	/admin/config/search/search_api/index/default_node_index/fields


ApacheSolr

    /admin/reports/apachesolr/solr


If the field is not yet indexed, add  it and re-index the site.

###Sort results

For usage with Search API sort the results _descending_ on relevance in the view that is used for searching:

	SORT CRITERIA
	Search: Relevance (desc)


###Test

Use the search page on

    /search/site/

for Apache Solr

